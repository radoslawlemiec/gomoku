package MinMax;

public interface GomokuAlgorithm {
	public int[] start(int[] lastChange, String playerSign);
}
