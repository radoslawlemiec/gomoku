package MinMax;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.IntPredicate;

import javax.print.attribute.standard.RequestingUserName;
import javax.swing.JButton;
import javax.swing.text.Position;

public class MinMax implements GomokuAlgorithm{

	private JButton[][] board;
	private int boardSize;
	private int player = 1;
	private int[][] myBoard;
	
	private int recursiveDepth = 3;
	
	
	// przechowuje puste pola wraz z ocen�mi jako�ci dla wyboru tego pola 
	// ocena jako�ci podzielona jest na 12 warto�ci int
	// [0] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [1] - liczba zgodnych kolorem pionk�w na ukos g�ra/prawo od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [2] - liczba zgodnych kolorem pionk�w w prawo od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [3] - liczba zgodnych kolorem pionk�w na ukos w prawo/d�l od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [4] - liczba zgodnych kolorem pionk�w w d� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [5] - liczba zgodnych kolorem pionk�w na ukos na d�/lewo od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [6] - liczba zgodnych kolorem pionk�w w lewo od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [7] - liczba zgodnych kolorem pionk�w na ukos w lewo/g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	
	// [8] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [9] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [10] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [11] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [12] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	// [13] - liczba zgodnych kolorem pionk�w w g�r� od rozpatrywanego pola, do 5 pozycji lub do trafienia na przeciwny kolor
	
	HashMap<int[], int[]> emptyFieldsWithMarks;
	
	public MinMax(JButton[][] gameBoard){
		boardSize = gameBoard.length;
		board = gameBoard;
		myBoard = new int[boardSize][boardSize];
		
		emptyFieldsWithMarks = new HashMap<>();
		for(int i = 0; i<boardSize; i++)
			for(int j = 0; j<boardSize; j++){
				emptyFieldsWithMarks.put(new int[]{i,j}, new int[15]);
			}
	}
	
	private void setFieldChoice(int[] choice, int player){
		myBoard[choice[0]][choice[1]] = player;
		emptyFieldsWithMarks.remove(choice);
	}
	
	private void unsetFieldChoice(int[] choice){
		myBoard[choice[0]][choice[1]] = 0;
		emptyFieldsWithMarks.put(choice, new int[15]);
	}
	
	public int[] start(int[] lastChange, String playerSign){

		player = (playerSign == "black" ? -1: 1);
		setFieldChoice(lastChange, player*(-1));
		
		double bestChoice = Double.MAX_VALUE*(-1);
		Iterator<Entry<int[], int[]>> iterator = emptyFieldsWithMarks.entrySet().iterator();
		int[] bestChoicePosition = new int[]{0,0};
				
		while(iterator.hasNext()){
			Map.Entry<int[], int[]> pair = (Map.Entry<int[], int[]>)iterator.next();
			double tempChoice = recursiveMinMax(pair.getKey(), 1, player);
			if(tempChoice > bestChoice){
				bestChoice = tempChoice;
				bestChoicePosition = pair.getKey();
			}
		}
		
		setFieldChoice(bestChoicePosition, player);
		return bestChoicePosition;
	}
	
	private double recursiveMinMax(int[] position, int currDepth, int player){
		if(currDepth >= recursiveDepth || emptyFieldsWithMarks.isEmpty()) return 0;
		
		setFieldChoice(position, player);
		double choiceValue = computeChoiceValue(position, player*(-1)) * Math.pow(-1, currDepth+1);
		
		if(Math.abs(choiceValue) == 100) return choiceValue;
		
		Iterator<Entry<int[], int[]>> iterator = emptyFieldsWithMarks.entrySet().iterator();
		while(iterator.hasNext()){
			Map.Entry<int[], int[]> pair = (Map.Entry<int[], int[]>)iterator.next();
			choiceValue += recursiveMinMax(pair.getKey(), currDepth+1, player*(-1));
		}
		unsetFieldChoice(position);
		return choiceValue;
	}
	
	
	/////////////////////////////////////
	////   Modu� oceny danego pola   ////
	/////////////////////////////////////
	//
	// TODO fukcja oceny: 
	
	
	private int computeChoiceValue(int[] position, int player){
		if(isWinningField(position, player)) return 100;
		
		
		return 0;
	}
	
	private int checkXAxis(int[] position, int player){
		int result = 0;	
		
		int sumInLine = 0;
		int i = 1;
		while(i < 4){
			if(position[0] + i > boardSize) result--;
			else if(myBoard[position[0] + i][position[1]] == player){
				
			}
			
			i++;
		}
		
		return result;
	}

}
