package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

import MinMax.GomokuAlgorithm;
import MinMax.MinMax;

public class Gomoku {

	private int sizeOfBoard;
    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private JButton[][] gomokuBoardSquares;
    private JPanel gomokuBoard;
    private final JLabel message = new JLabel("Gomoku is ready to play! White go first.");
    private static final String COLS = "ABCDEFGHIJKLMNOPRS";

    private ImageIcon emptyIcon = new ImageIcon(new ImageIcon("src/images/emptyField.png").getImage().getScaledInstance(64, 64, Image.SCALE_DEFAULT));
    private ImageIcon whiteIcon = new ImageIcon(new ImageIcon("src/images/fieldWhite.png").getImage().getScaledInstance(64, 64, Image.SCALE_DEFAULT));
    private ImageIcon blackIcon = new ImageIcon(new ImageIcon("src/images/fieldBlack.png").getImage().getScaledInstance(64, 64, Image.SCALE_DEFAULT));
    private String currentPlayer = "White";
    private JButton lastClick = null;
    private int filledFields = 0;
    
    private GomokuAlgorithm player1 = null;
    private GomokuAlgorithm player2 = null;
    
    private ActionListener clickListener = new ActionListener(){
		@Override
		public void actionPerformed(ActionEvent e) {
			if(filledFields >= gomokuBoardSquares.length * gomokuBoardSquares.length) return;
			JButton click = (JButton)e.getSource();
			if(click.getIcon() == emptyIcon)
			{
				filledFields++;
				lastClick = click;
				if(currentPlayer.equals("Black")) {
					click.setIcon(blackIcon);
					if (checkWin()) return;
					currentPlayer = "White";
					message.setText("Current turn: " + currentPlayer + " " + getFieldColor(click));
					if(player1 != null) {
						int[] move = player1.start(getButtonPosition(lastClick), currentPlayer);
						gomokuBoardSquares[move[0]][move[1]].doClick();
					}
				} else {
					click.setIcon(whiteIcon); 
					if(checkWin()) return;
					currentPlayer = "Black";
					message.setText("Current turn: " + currentPlayer);
					if(player2 != null) {
						int[] move = player2.start(getButtonPosition(lastClick), currentPlayer);
						gomokuBoardSquares[move[0]][move[1]].doClick();
					}
				}
			} else
				message.setText("Current turn: " + currentPlayer + " Choose empty field!");
		}
    };
    
    public Gomoku(int sizeOfBoard) {
    	this.sizeOfBoard = sizeOfBoard;
    	gomokuBoardSquares = new JButton[sizeOfBoard][sizeOfBoard];
        initializeGui();
    }
    
    private int[] getButtonPosition(JButton jBut){
    	int x=0, y=0;
    	while(gomokuBoardSquares[x][y] != jBut){
    		if(x < sizeOfBoard - 1) x++;
    		else if(y < sizeOfBoard - 1){
    			y++;
    			x = 0;
    		}
    	}
    	return new int[]{x,y};
    }
    
    private int[] getFieldXY(JButton jBut){
    	int[] result = new int[]{-1,-1};
    	for(int x=0; x<gomokuBoardSquares.length; x++)
    		for(int y=0; y<gomokuBoardSquares.length; y++)
    			if(gomokuBoardSquares[x][y] == jBut){
    				result[0] = x;
    				result[1] = y;
    				return result;
    			}
    	return result;
    }
    
    public String getFieldColor(int x, int y){
    	return gomokuBoardSquares[x][y].getIcon() == blackIcon ? "black": "white";
    }
    
    public String getFieldColor(JButton jb){
    	return jb.getIcon() == blackIcon ? "black": "white";
    }
    
    private boolean checkWin(){
    	
    	int[] position = getFieldXY(lastClick);
    	
    	//firs check in row
    	int i = 1;
    	int sumOfFits = 1;
    	while(i<=5 && position[0]-i>=0 && gomokuBoardSquares[position[0]-i][position[1]].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	i = 1;
    	while(i<=5 && position[0]+i<gomokuBoardSquares.length && gomokuBoardSquares[position[0]+i][position[1]].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	if(sumOfFits == 5){
    		message.setText("Game is over. Won: " + currentPlayer);
    		filledFields = Integer.MAX_VALUE;
    		return true;
    	}
    	
    	//second check in column
    	i = 1;
    	sumOfFits = 1;
    	while(i<=5 && position[1]-i>=0 && gomokuBoardSquares[position[0]][position[1]-i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	i = 1;
    	while(i<=5 && position[1]+i<gomokuBoardSquares.length && gomokuBoardSquares[position[0]][position[1]+i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	if(sumOfFits == 5){
    		message.setText("Game is over. Won: " + currentPlayer);
    		filledFields = Integer.MAX_VALUE;
    		return true;
    	}
    	
    	//third check in left bottom to top right diagonal
    	i = 1;
    	sumOfFits = 1;
    	while(i<=5 && position[0]-i>=0 && position[1]-i>=0 && gomokuBoardSquares[position[0]-i][position[1]-i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	i = 1;
    	while(i<=5 && position[0]+i<gomokuBoardSquares.length && position[1]+i<gomokuBoardSquares.length && gomokuBoardSquares[position[0]+i][position[1]+i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	if(sumOfFits == 5){
    		message.setText("Game is over. Won: " + currentPlayer);
    		filledFields = Integer.MAX_VALUE;
    		return true;
    	}
    	
    	//fourth check in top left to bottom right diagonal
    	i = 1;
    	sumOfFits = 1;
    	while(i<=5 && position[0]-i>=0 && position[1]+i<gomokuBoardSquares.length && gomokuBoardSquares[position[0]-i][position[1]+i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	i = 1;
    	while(i<=5 && position[0]+i<gomokuBoardSquares.length && position[1]-i>=0 && gomokuBoardSquares[position[0]+i][position[1]-i].getIcon() == lastClick.getIcon()){
    		sumOfFits++;
    		i++;
    	}
    	if(sumOfFits == 5){
    		message.setText("Game is over. Won: " + currentPlayer);
    		filledFields = Integer.MAX_VALUE;
    		return true;
    	}
    	
    	//last check if not a draw
    	if(filledFields == gomokuBoardSquares.length*gomokuBoardSquares.length){
    		message.setText("Game is over. DRAW!");
    		return true;
    	}
    	return false;
    }
    
    private final void initializeGui() {
        // set up the main GUI
        gui.setBorder(new EmptyBorder(0, 0, 0, 0));
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        gui.add(tools, BorderLayout.PAGE_START);

        JButton newB = new JButton("New");
        newB.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
		    	for(int i=0; i<gomokuBoardSquares.length; i++)
		    		for(int j=0; j<gomokuBoardSquares.length; j++)
		    			gomokuBoardSquares[i][j].setIcon(emptyIcon);
		    	filledFields = 0;
		    	currentPlayer = "O";
		    	message.setText("Gomoku is ready to play! White go first.");
		    	
			}
		});
        tools.add(newB);
        
        JButton newBMinMax = new JButton("MinMax algorithm");
        newBMinMax.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(player1 == null) {
					player1 = new MinMax(gomokuBoardSquares);
					message.setText("Set player1 to MinMax algorithm.");
				}
				else if(player2 == null) {
					player2 = new MinMax(gomokuBoardSquares);
					message.setText("Set player2 to MinMax algorithm.");
				}
		    	
		    	
			}
		});
        tools.add(newBMinMax);
        
        JButton newBClearPlayers = new JButton("Clear players");
        newBClearPlayers.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				player1 = null;
				player2 = null;
				message.setText("Set player2 to MinMax algorithm.");
			}
		});
        tools.add(newBClearPlayers);
        
        tools.add(new JButton("Save")); // TODO - add functionality!
        tools.addSeparator();
        tools.add(new JButton("Resign")); // TODO - add functionality!
        tools.addSeparator();
        tools.add(message);
        gui.add(new JLabel("?"), BorderLayout.LINE_START);
        gomokuBoard = new JPanel(new GridLayout(0, gomokuBoardSquares.length + 1));
        gomokuBoard.setBorder(new LineBorder(Color.WHITE));
        gui.add(gomokuBoard);

        // create the board squares
        Insets buttonMargin = new Insets(0,0,0,0);
        for (int ii = 0; ii < gomokuBoardSquares.length; ii++) {
            for (int jj = 0; jj < gomokuBoardSquares[ii].length; jj++) {
                JButton b = new JButton();
                b.addActionListener(clickListener);
                b.setMargin(buttonMargin);
                b.setIcon(emptyIcon);
                b.setBackground(Color.WHITE);
                gomokuBoardSquares[jj][ii] = b;
            }
        }

        //fill the board
        gomokuBoard.add(new JLabel(""));
        // fill the top row
        for (int ii = 0; ii < gomokuBoardSquares.length; ii++)
            gomokuBoard.add(new JLabel(COLS.substring(ii, ii + 1), SwingConstants.CENTER));
        // fill the left column
        for (int ii = 0; ii < gomokuBoardSquares.length; ii++) {
            for (int jj = 0; jj < gomokuBoardSquares.length; jj++) {
                switch (jj) {
                    case 0:
                        gomokuBoard.add(new JLabel("" + (ii + 1), SwingConstants.CENTER));
                    default:
                        gomokuBoard.add(gomokuBoardSquares[jj][ii]);
                }
            }
        }
    }

    public final JComponent getGomokuBoard() {return gomokuBoard;}

    public final JComponent getGui() {return gui;}

    public static void main(String[] args) {
        Runnable r = new Runnable() {

            @Override
            public void run() {
                Gomoku cb = new Gomoku(9);

                JFrame f = new JFrame("Gomoku");
                f.add(cb.getGui());
                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                f.setLocationByPlatform(true);

                // ensures the frame is the minimum size it needs to be
                // in order display the components within it
                f.pack();
                // ensures the minimum size is enforced.
                f.setMinimumSize(f.getSize());
                f.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(r);
    }
}